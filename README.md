# CSS-Grid
Grid in general terms is a set of intersection of horizontal and vertical lines forming rows and columns.

## About CSS-Grid
CSS grid is a 2 dimensional layout system where the elements can be placed within the rows and columns defined by the grid.
A general representation of CSS-grid layout is as below:-

<img src="./img/cssGridLayout.png" width="300px">

## Some Common terminologies associated with CSS-Grid


**Grid Container** \
It is the parent of all grid items.
It's created when **display:grid** is applied on an element.

**Grid Item** \
The items inside the grid container are grid items.These are the children of grid container.

**Grid Line** \
These are the horizontal and vertical lines that form the basis of the grid structure. In the image below the numbers along x and y axis are the numerical index.Negative indexing denotes last line from the reference.

<img src="./img/gridLines.png " width="200px" height="150px">

**Grid Cell** \
A grid cell is the smallest unit on a grid. It is the space between 2 adjacent row grid lines and 2 adjacent column grid lines.

**Grid Track** \
The space within two adjacent grid lines is a grid track. Grid tracks can be separated using ***gutters*** by using grid-row-gap and grid-column-gap properties.


**Grid Area** \
It can be understood as the combined space occupied by one or more cells.Grid areas must be rectangular.



Let's begin with creating a minimal html and css file which will further be styled using CSS-Grid.

```
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./style.css">
    <title>CSS-Grid</title>
</head>
<body>

</body>
</html>
```
<br>
A grid container can now be created to enclose the grid items.
<br>

```
 <div class="container">
        <div class="grid-item grid-item-1">grid-item-1</div>
        <div class="grid-item grid-item-2">grid-item-2</div>
        <div class="grid-item grid-item-3">grid-item-3</div>
        <div class="grid-item grid-item-4">grid-item-4</div>
        <div class="grid-item grid-item-5">grid-item-5</div>
 </div>
```
With a little touch of CSS we have
<br>
<img src="./img/css1.png" >

To get started and create a grid
we select the container class  and apply **display:grid**.

Rows and Columns for the grid can now be defined to manage the size of grid cells. It can be done using **grid-template-rows** and **grid-template-columns**

px can be used to create cells of fixed sizes.
```
.container {
    display: grid;
    grid-template-columns: 300px 200px;
    grid-template-rows: 100px 100px 100px;

  }
  ```
<img src="./img/css2.png" >

fr,fraction units can be used to create cells of flexible sizes,as it is done using flex grow in flex-box.

 ```
.container {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-template-rows: 100px 100px 100px;
  }
  ```
Columns on the left therefore takes twice the size than the columns on right.<br>
  <img src="./img/css3.png">
<br>
  To make the cells scalable with respect to the content inside, **minmax** comes handy.

  ```
  .container {
    display: grid;
    grid-template-columns: 2fr 1fr;
    grid-auto-rows: minmax(150px,auto);
  }
  ```
  <br>

  <img src ="./img/css4.png">

  Using **grid-auto-rows: minmax(150px,auto);** sets the min height of a row to be of 150px and maximum to be just large enough accommodate content.

Gaps between the rows and columns can be created using:

```
grid-row-gap:20px;
grid-column-gap: 20px;

```
<img src="./img/css6.png">

The same can also be done using:

```
grid-gap:20px
```
Css grids enables us to create a layout like this:
<img src="./img/css7.png">
<br>
***or this:***
<br>
<img src="./img/css8.png">

This can done using **grid-row-start** , **grid-row-end** and
**grid-column-start** , **grid-column-end** where start and end signify the span of the grid cells along rows and columns.

```

  .grid-item-1{
      grid-column-start: 1;
      grid-column-end:4;
      grid-row-start:1 ;
      grid-row-end:2 ;

  }
  .grid-item-2{
    grid-column-start: 1;
    grid-column-end:1;
    grid-row-start:2 ;
    grid-row-end:4 ;
}
.grid-item-3{
    grid-column-start: 2;
    grid-column-end:3;
    grid-row-start:2 ;
    grid-row-end:4 ;
}
.grid-item-4{
    grid-column-start: 3;
    grid-column-end:4;
    grid-row-start:2 ;
    grid-row-end:4 ;
}
.grid-item-5{
    grid-column-start: 1;
    grid-column-end:4;

}
```
<br>
will produce the following layout
<br>


<img src="./img/css9.png">

The scope of the article is limited to the introduction to CSS-Grid and deals with the subject superficially.CSS-Grid is a very powerful tool and has made lives a lot easier.More on it can be found on [css-tricks](https://css-tricks.com) ,which is a great resource for CSS in general.



### References
[codrops](https://tympanus.net/codrops/css_reference/grid/)<br>
[mdn docs](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout)
